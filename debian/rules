#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

MAKE_OVERRIDE = AT= QUIET_CXX= QUIET_CC= QUIET_AR= ABI_FLAG='$(LDFLAGS) $(CPPFLAGS) $(CFLAGS)'

ifneq ($(DEB_BUILD_ARCH),$(DEB_HOST_ARCH))
include /usr/share/dpkg/buildtools.mk
CONFIGURE_FLAGS = --=$(DEB_HOST_GNU_CPU)-$(DEB_HOST_ARCH_OS) CC=$(CC) CXX=$(CXX) AR=$(AR)
endif

%:
	dh $@ --with sphinxdoc

# upstream Makefile has only CFLAGS, not CPPFLAGS and not even CXXFLAGS, so
# inject flags using configure. let's hope CFLAGS will always be good enough
# even for $(CXX)
override_dh_auto_configure:
	./configure --prefix=/usr --disable-static --with-flint=/usr CFLAGS='$(CPPFLAGS) $(CFLAGS)' $(CONFIGURE_FLAGS)
	sed -i Makefile -e "s/libarb/libflint-arb/g"
	sed -i Makefile -e "s/-larb/-lflint-arb/g"
	sed -i Makefile -e "s|LIBDIR=lib|LIBDIR=lib/$(DEB_HOST_MULTIARCH)|g"

override_dh_auto_build:
	dh_auto_build -- $(MAKE_OVERRIDE)
	make -C doc/ html
	find doc/ -type f -name "*.html" | \
		xargs sed -i -e "s|https://cdnjs.cloudflare.com/ajax/libs/mathjax/.*/latest.js|file:///usr/share/javascript/mathjax/MathJax.js|g"

override_dh_auto_test:
	dh_auto_test -- $(MAKE_OVERRIDE)
